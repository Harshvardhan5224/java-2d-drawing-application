

package java2d_app;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class JAVA2D_APP extends JFrame {

    static String shape, color;
    static JCheckBox fillBox;
    static JCheckBox dashBox;
    static JCheckBox gradientBox;

    public static void main(String[] args) {

        JAVA2D_APP application = new JAVA2D_APP();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.setSize(1000, 600);
        application.setVisible(true);
    }
    private final JButton undoJButton;
    private final JButton clearJButton;
    private final JComboBox shapes;
    private JButton changeFirstColorJButton;
    private JButton changeSecondColorJButton;
    private final BorderLayout layout;
    private final JLabel statusBar;
    private final DrawPanel drawPanel;
    private final JTextField lineWidthJTextField;
    private final JTextField dashLengthJTextField;
    Point start, end;

    public JAVA2D_APP() {

        super("Paint Program");
        layout = new BorderLayout();
        setLayout(layout);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        add(topPanel, BorderLayout.NORTH);

        drawPanel = new DrawPanel();
        add(drawPanel, BorderLayout.CENTER);

        JPanel cursorPanel = new JPanel();
        add(cursorPanel, BorderLayout.SOUTH);
        cursorPanel.setLayout(new BorderLayout());

        JPanel topLabels = new JPanel();
        JPanel bottomLabels = new JPanel();
        topPanel.add(topLabels, BorderLayout.NORTH);
        topPanel.add(bottomLabels, BorderLayout.SOUTH);
        
        undoJButton = new JButton("Undo");
        topLabels.add(undoJButton);

        clearJButton = new JButton("Clear");
        topLabels.add(clearJButton);

        undoJButton.addActionListener(ae -> drawPanel.undo());
        clearJButton.addActionListener(ae -> drawPanel.clear());
        JLabel shapeChoiceJLabel = new JLabel("Shape: ");
        topLabels.add(shapeChoiceJLabel);

        shapes = new JComboBox(new String[]{"Line", "Oval", "Rectangle"});
        shapes.setSelectedItem(drawPanel.getShapeType());
        topLabels.add(shapes);
        shapes.addActionListener((ae) -> {
            JComboBox cb = (JComboBox) ae.getSource();
            drawPanel.setShapeType((String) cb.getSelectedItem());
        });

        fillBox = new JCheckBox("Filled");
        fillBox.setSelected(drawPanel.isFilled());
        topLabels.add(fillBox);

        gradientBox = new JCheckBox("Use Gradient");
        gradientBox.setSelected(drawPanel.useGradient());
        bottomLabels.add(gradientBox);

        changeFirstColorJButton = new JButton("1st Color...");
        bottomLabels.add(changeFirstColorJButton, BorderLayout.SOUTH);
        changeFirstColorJButton.addActionListener(event -> {
            Color colorOne = drawPanel.getColorOne();
            colorOne = JColorChooser.showDialog(this, "Choose a color", colorOne);
            if (colorOne != null) {
                drawPanel.setColorOne(colorOne);
            }
            changeFirstColorJButton.setBackground(colorOne);
        });

        changeSecondColorJButton = new JButton("2nd Color...");
        bottomLabels.add(changeSecondColorJButton, BorderLayout.SOUTH);
        changeSecondColorJButton.addActionListener(event -> {
            Color colorTwo = drawPanel.getColorTwo();
            colorTwo = JColorChooser.showDialog(this, "Choose a color", colorTwo);
            if (colorTwo != null) {
                drawPanel.setColorTwo(colorTwo);
            }
            changeSecondColorJButton.setBackground(colorTwo);
        });

        JLabel lineJLabel = new JLabel("Line Length: ");
        bottomLabels.add(lineJLabel);
        lineJLabel.setFont(lineJLabel.getFont().deriveFont(14.0f));
        lineWidthJTextField = new JTextField(String.format("%d", drawPanel.getLineWidth()), 2);
        bottomLabels.add(lineWidthJTextField);

        JLabel dashJLabel = new JLabel("Dash Length: ");
        bottomLabels.add(dashJLabel);
        dashJLabel.setFont(dashJLabel.getFont().deriveFont(14.0f));
        dashLengthJTextField = new JTextField(String.format("%d", drawPanel.getDashLength()), 2);
        bottomLabels.add(dashLengthJTextField);

        dashBox = new JCheckBox("Dashed");
        bottomLabels.add(dashBox);
        fillBox.addActionListener(ae -> drawPanel.setFilled(((JCheckBox) ae.getSource()).isSelected()));
        fillBox.setSelected(false);
        gradientBox.addActionListener(ae -> drawPanel.setUseGradient(((JCheckBox) ae.getSource()).isSelected()));
        lineWidthJTextField.addActionListener(ae -> drawPanel.setLineWidth(Integer.parseInt(((JTextField) ae.getSource()).getText())));
        dashLengthJTextField.addActionListener(ae -> drawPanel.setDashLength(Integer.parseInt(((JTextField) ae.getSource()).getText())));
        dashBox.addActionListener(ae -> drawPanel.setDashed(((JCheckBox) ae.getSource()).isSelected()));
        statusBar = new JLabel("");
        cursorPanel.add(statusBar, BorderLayout.WEST);

    }

    private class DrawPanel extends JPanel {

        private ArrayList<Shape> itemsDrawn;
        private String shapeType;
        private boolean isFilled;
        private boolean useGradient;
        private Color colorOne;
        private Color colorTwo;
        private int lineWidth;
        private int dashLength;
        private boolean isDashed;

        public DrawPanel() {
            itemsDrawn = new ArrayList<>();
            shapeType = "Line";
            isFilled = false;
            useGradient = false;
            colorOne = Color.BLACK;
            colorTwo = Color.BLUE;
            lineWidth = 5;
            dashLength = 10;
            isDashed = false;
            MouseHandler handler = new MouseHandler();
            addMouseListener(handler);
            addMouseMotionListener(handler);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            this.setBackground(Color.WHITE);

            Graphics2D g2d = (Graphics2D) g.create();
            for (Shape s : itemsDrawn) {
                s.draw(g2d);
            }
            g2d.dispose();
        }

        private class MouseHandler implements MouseListener, MouseMotionListener {

            @Override
            public void mouseMoved(MouseEvent event) {
                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

            @Override
            public void mouseExited(MouseEvent event) {
                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

            @Override
            public void mouseEntered(MouseEvent event) {
                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

            @Override
            public void mouseReleased(MouseEvent event) {
                repaint();
                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

            @Override
            public void mousePressed(MouseEvent event) {
                Point p1 = event.getPoint();
                Point p2 = new Point((int) p1.getX() + 15, (int) p1.getY() + 15); // Used for gradient pattern
                Paint paint;
                if (useGradient()) {
                    paint = new GradientPaint(p1, getColorOne(), p2, getColorTwo(), true);
                } else {
                    paint = getColorOne();
                }
                BasicStroke stroke;
                if (isDashed()) {
                    stroke = new BasicStroke(getLineWidth(), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10, new float[]{getDashLength()}, 0);
                } else {
                    stroke = new BasicStroke(getLineWidth(), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
                }
                Shape shape;
                switch (getShapeType().toUpperCase()) {
                    // Use p1 for second point because mouse has not moved yet
                    case "LINE":
                        shape = new MyLine(p1, p1, paint, stroke);
                        break;
                    case "OVAL":
                        shape = new MyOval(p1, p1, isFilled(), paint, stroke);
                        break;
                    case "RECTANGLE":
                        shape = new MyRectangle(p1, p1, isFilled(), paint, stroke);
                        break;
                    default: // Default to a line
                        shape = new MyLine(p1, p1, paint, stroke);
                }
                itemsDrawn.add(shape);
                repaint();

                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

            @Override
            public void mouseClicked(MouseEvent event) {
                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

            @Override
            public void mouseDragged(MouseEvent event) {
                int lastIndex = itemsDrawn.size() - 1;
                if (lastIndex >= 0) {
                    Shape currentShape = itemsDrawn.get(lastIndex);
                    if (currentShape instanceof MyLine) {
                        ((MyLine) currentShape).setEndPoint(event.getPoint());
                    } else {
                        ((MyBoundedShape) currentShape).setHeightAndWidth(event.getPoint());
                    }
                }
                repaint();
                statusBar.setText(String.format("[%d, %d]", event.getX(), event.getY()));
            }

        }

        public void undo() {
            if (itemsDrawn.size() > 0) {
                itemsDrawn.remove(itemsDrawn.size() - 1);
            }
            repaint();
        }

        public void clear() {
            itemsDrawn.clear();
            repaint();
        }

        String getShapeType() {
            return shapeType;
        }

        public void setShapeType(String shapeType) {
            this.shapeType = shapeType;
        }

        boolean isFilled() {
            return isFilled;
        }

        public void setFilled(boolean filled) {
            isFilled = filled;
        }

        boolean useGradient() {
            return useGradient;
        }

        public void setUseGradient(boolean useGradient) {
            this.useGradient = useGradient;
        }

        Color getColorOne() {
            return colorOne;
        }

        public void setColorOne(Color changeFirstColorJButton) {
            this.colorOne = changeFirstColorJButton;
        }

        Color getColorTwo() {
            return colorTwo;
        }

        public void setColorTwo(Color changeSecondColorJButton) {
            this.colorTwo = changeSecondColorJButton;
        }

        int getLineWidth() {
            return lineWidth;
        }

        public void setLineWidth(int lineWidth) {
            this.lineWidth = lineWidth;
        }

        int getDashLength() {
            return dashLength;
        }

        public void setDashLength(int dashLength) {
            this.dashLength = dashLength;
        }

        boolean isDashed() {
            return isDashed;
        }

        public void setDashed(boolean dashed) {
            isDashed = dashed;
        }

    }

    //main base class for shapes
    public abstract class Shape {

        private Point startPoint;
        private Paint paint;
        private BasicStroke stroke;

        public Shape(Point p1, Paint paint, BasicStroke stroke) {
            startPoint = p1;
            this.paint = paint;
            this.stroke = stroke;
        }

        public abstract void draw(Graphics2D g);

        public Point getStartPoint() {
            return startPoint;
        }

        public void setStartPoint(Point p) {
            if (p.getX() >= 0 && p.getY() >= 0) {
                startPoint = p;
            } else {
                if (startPoint == null) {
                    startPoint = new Point(0, 0); // Initialize Point if not already initialized
                }
            }
        }

        public Paint getPaint() {
            return paint;
        }

        public void setPaint(Paint paint) {
            this.paint = paint;
        }

        public BasicStroke getStroke() {
            return stroke;
        }

        public void setStroke(BasicStroke stroke) {
            this.stroke = stroke;
        }
    }

    public class MyLine extends Shape {

        private Point endPoint;

        public MyLine(Point p1, Point p2, Paint paint, BasicStroke stroke) {
            super(p1, paint, stroke);
            if (p2.getX() >= 0 && p2.getY() >= 0) {
                endPoint = p2;
            } else {
                if (endPoint == null) {
                    endPoint = new Point(0, 0);
                }
            }
        }

        public Point getEndPoint() {
            return endPoint;
        }

        public void setEndPoint(Point p) {
            if (p.getX() >= 0 && p.getY() >= 0) {
                endPoint = p;
            } else {
                if (endPoint == null) {
                    endPoint = new Point(0, 0);
                }
            }
        }

        @Override
        public void draw(Graphics2D g) {
            g.setPaint(getPaint());
            g.setStroke(getStroke());
            Point p1 = getStartPoint();
            Point p2 = getEndPoint();
            g.drawLine((int) p1.getX(), (int) p1.getY(), (int) p2.getX(), (int) p2.getY());
        }
    }

    public abstract class MyBoundedShape extends Shape {

        private int width;
        private int height;
        private boolean isFilled;

        public MyBoundedShape(Point p1, Point p2, boolean isFilled, Paint paint, BasicStroke stroke) {
            super(p1, paint, stroke);
            if (p2 != null) {
                // 2nd point - 1st point
                width = (int) (p2.getX() - p1.getX());
                height = (int) (p2.getY() - p1.getY());
            }
            this.isFilled = isFilled;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public boolean isFilled() {
            return isFilled;
        }

        public void setFilled(boolean filled) {
            isFilled = filled;
        }

        public void setHeightAndWidth(Point p) {
            if (p != null) {
                // 2nd point - 1st point
                setWidth((int) (p.getX() - getStartPoint().getX()));
                setHeight((int) (p.getY() - getStartPoint().getY()));
            }
        }

        protected int[] getDrawParameters() {
            Point p1;
            int w = getWidth();
            int h = getHeight();
            if (w < 0 && h < 0) {
                p1 = new Point((int) getStartPoint().getX() + w, (int) getStartPoint().getY() + h);
                w = w * -1;
                h = h * -1;
            } else if (w < 0) {
                p1 = new Point((int) getStartPoint().getX() + w, (int) getStartPoint().getY());
                w = w * -1;
            } else if (h < 0) {
                p1 = new Point((int) getStartPoint().getX(), (int) getStartPoint().getY() + h);
                h = h * -1;
            } else {
                p1 = getStartPoint();
            }
            return new int[]{(int) p1.getX(), (int) p1.getY(), w, h};
        }
    }

    public class MyRectangle extends MyBoundedShape {

        public MyRectangle(Point p1, Point p2, boolean isFilled, Paint paint, BasicStroke stroke) {
            super(p1, p2, isFilled, paint, stroke);
        }

        @Override
        public void draw(Graphics2D g) {
            g.setPaint(getPaint());
            g.setStroke(getStroke());
            int[] drawParam = getDrawParameters();
            if (isFilled()) {
                g.fillRect(drawParam[0], drawParam[1], drawParam[2], drawParam[3]);
            } else {
                g.drawRect(drawParam[0], drawParam[1], drawParam[2], drawParam[3]);
            }
        }
    }

    public class MyOval extends MyBoundedShape {

        public MyOval(Point p1, Point p2, boolean isFilled, Paint paint, BasicStroke stroke) {
            super(p1, p2, isFilled, paint, stroke);
        }

        @Override
        public void draw(Graphics2D g) {
            g.setPaint(getPaint());
            g.setStroke(getStroke());
            int[] drawParam = getDrawParameters();
            if (isFilled()) {
                g.fillOval(drawParam[0], drawParam[1], drawParam[2], drawParam[3]);
            } else {
                g.drawOval(drawParam[0], drawParam[1], drawParam[2], drawParam[3]);
            }
        }
    }

}

